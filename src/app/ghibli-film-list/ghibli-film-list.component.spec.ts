import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GhibliFilmListComponent } from './ghibli-film-list.component';

describe('GhibliFilmListComponent', () => {
  let component: GhibliFilmListComponent;
  let fixture: ComponentFixture<GhibliFilmListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GhibliFilmListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GhibliFilmListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
