import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GhibliService {
  baseUrl: string = environment.apiUrl;

  ghibliFilms$: Observable<any> = this.http.get<any>(`${this.baseUrl}/films`);

  constructor(private http: HttpClient) { }

}
