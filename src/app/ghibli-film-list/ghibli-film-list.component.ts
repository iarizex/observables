import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { GhibliService } from '../services/ghibli.service';

@Component({
  selector: 'app-ghibli-film-list',
  templateUrl: './ghibli-film-list.component.html',
  styleUrls: ['./ghibli-film-list.component.css']
})
export class GhibliFilmListComponent implements OnInit {

  constructor(private ghibliService: GhibliService) { }

  filmList$: Observable<any> = this.ghibliService.ghibliFilms$;

  ngOnInit(): void {
  }

}
